# redmine-plugin-work-report-exporter
# Copyright (C) 2022  ClearCode Inc.
# Copyright (C) 2024  Sutou Kouhei <kou@clear-code.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

module WorkReportExporter
  class Customer
    def initialize(project)
      @project = project
      @settings = Setting.plugin_work_report_exporter
    end

    def company_name
      @company_name ||= project_field_value(:company_name)
    end

    def company_title
      @company_title ||= project_field_value(:company_title)
    end

    def provider_name
      @provider_name ||= project_field_value(:provider_name)
    end

    def comment_sanitizer
      @comment_sanitizer ||= project_field_value(:comment_sanitizer)
    end

    def effective_versions
      now = User.current.today
      @project.versions.reject do |version|
        version.effective_date.nil? or version.effective_date < now
      end.sort do |version|
        version.effective_date
      end
    end

    private
    def project_field_value(name)
      field_name = @settings.field_name(name)
      return @settings.default_value(name) if field_name.blank?

      format = Settings::PROJECT_ITEMS[name][:field_format] || "string"
      field = ProjectCustomField
                .where(name: field_name,
                       field_format: format)
                .first
      return @settings.default_value(name) if field.nil?

      value = @project.custom_field_value(field.id)
      value = @settings.cast(value, format)
      value = @settings.default_value(name) if value.blank?
      value
    end
  end
end
